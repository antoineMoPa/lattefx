class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@cutvid.io'
  layout 'mailer'
end
