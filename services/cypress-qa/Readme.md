# Cypress tests

```
npm install
```

You can now open Cypress by running:

```
node_modules/.bin/cypress open
```

# Run headless

```
node_modules/.bin/cypress run
```